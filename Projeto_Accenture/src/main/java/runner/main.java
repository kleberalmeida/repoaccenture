package runner;

import navigation.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import utils.Validacao;

public class main {

    private static WebDriver driver;

    public static void main(String[] args) throws Exception {

        System.setProperty("webdriver.chrome.driver", "C:\\Windows\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        AcessarUrl.acessarUrl(driver);
        EnterVehicle.cadastroEnterVehicle(driver);
        EnterInsurant.cadastroEnterInsurant(driver);
        EnterProduct.cadastroEnterProduct(driver);
        SelectPrice.selectPrice(driver);
        SendQuote.cadastroSendQuote(driver);
        Validacao.validarMensagem(driver);

        driver.quit();


    }
}
