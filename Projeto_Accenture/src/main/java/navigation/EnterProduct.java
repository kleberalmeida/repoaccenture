package navigation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class EnterProduct {

    public static void cadastroEnterProduct(WebDriver driver){

        driver.findElement(By.id("startdate")).sendKeys("10/02/2021");
        Select insurance = new Select(driver.findElement(By.id("insurancesum")));
        insurance.selectByValue("3000000");
        Select merit = new Select(driver.findElement(By.id("meritrating")));
        merit.selectByValue("Bonus 1");
        Select damage = new Select(driver.findElement(By.id("damageinsurance")));
        damage.selectByValue("No Coverage");
        driver.findElement(By.xpath("//*[@id=\"insurance-form\"]/div/section[3]/div[5]/p/label[1]/span")).click();
        driver.findElement(By.id("courtesycar")).sendKeys("yes");
        driver.findElement(By.id("nextselectpriceoption")).click();



    }
}
