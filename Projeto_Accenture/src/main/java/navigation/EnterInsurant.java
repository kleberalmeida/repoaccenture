package navigation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EnterInsurant {


    public static void cadastroEnterInsurant(WebDriver driver){

        driver.findElement(By.id("firstname")).sendKeys("teste");
        driver.findElement(By.id("lastname")).sendKeys("testeqa");
        driver.findElement(By.id("birthdate")).sendKeys("01/06/1980");
        driver.findElement(By.xpath("//*[@id=\"insurance-form\"]/div/section[2]/div[4]/p/label[1]/span")).click();
        driver.findElement(By.id("streetaddress")).sendKeys("rua teste");
        driver.findElement(By.id("country")).sendKeys("brazil");
        driver.findElement(By.id("zipcode")).sendKeys("06454000");
        driver.findElement(By.id("city")).sendKeys("São Paulo");
        driver.findElement(By.id("occupation")).sendKeys("Employee");
        driver.findElement(By.xpath("//*[@id=\"insurance-form\"]/div/section[2]/div[10]/p/label[1]/span")).click();
        driver.findElement(By.id("website")).sendKeys("www.teste.com.br");
        String fileimagem = "C:\\Testetecnico\\imagem\\imagem.png";
        WebElement imagem = driver.findElement(By.id("picture"));
        imagem.sendKeys(fileimagem);
        driver.findElement(By.id("nextenterproductdata")).click();


    }
}
