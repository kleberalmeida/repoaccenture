package navigation;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class EnterVehicle {


    public static void cadastroEnterVehicle(WebDriver driver){


        driver.findElement(By.id("make")).sendKeys("volvo");
        driver.findElement(By.id("model")).sendKeys("scooter");
        driver.findElement(By.id("cylindercapacity")).sendKeys("1");
        driver.findElement(By.id("engineperformance")).sendKeys("1");
        driver.findElement(By.id("dateofmanufacture")).sendKeys("01/01/2021");
        driver.findElement(By.id("numberofseats")).sendKeys("1");
        driver.findElement(By.xpath("//*[@id=\"insurance-form\"]/div/section[1]/div[7]/p/label[1]/span")).click();
        driver.findElement(By.id("numberofseatsmotorcycle")).sendKeys("1");
        driver.findElement(By.id("fuel")).sendKeys("petrol");
        driver.findElement(By.id("payload")).sendKeys("1");
        driver.findElement(By.id("totalweight")).sendKeys("100");
        driver.findElement(By.id("listprice")).sendKeys("500");
        driver.findElement(By.id("licenseplatenumber")).sendKeys("1");
        driver.findElement(By.id("annualmileage")).sendKeys("100");
        driver.findElement(By.id("nextenterinsurantdata")).click();

    }
}
